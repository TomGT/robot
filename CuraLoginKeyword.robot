*** Settings ***
Documentation    Un premier test d'accès à la page de login

Resource    ./resources/menu.resource
Resource    ./resources/home.resource
Resource    ./resources/login.resource
Resource    ./resources//appointment_form.resource

Library    SeleniumLibrary

Test Setup    MySetup
Test Teardown    Close Browser

*** Variables ***
${url}    https://katalon-demo-cura.herokuapp.com/
${browser}    firefox

*** Keywords ***
MySetup
    # Open Browser and go to Cura Healthcare
    Navigate to Cura Healthcare
        # Implicit wait
    Set Selenium Implicit Wait    2 seconds
    
Navigate to Cura Healthcare
    # Open firefox and go to Cura Healthcare
    Open Browser    ${url}    ${browser}
    # Verifying title
    Title Should Be    CURA Healthcare Service
    # Maximize window, to be sure that everything goes well
    Maximize Browser Window

*** Test Cases ***
Login with existing user
    Go To Login
    # Whole test
    Login    John Doe    ThisIsNotAPassword
    Fill Form    02/07/2023    My personnal comment for test
    Go To Main Page

Login with wrong user
    Go To Login
    # Whole test
    Login    Michel    Michel
    Page Should Contain    Login failed! 
