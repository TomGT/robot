*** Settings ***
Documentation    Un premier test d'accès à la page de login
Library    SeleniumLibrary

*** Variables ***
${url}    https://katalon-demo-cura.herokuapp.com/
${browser}    firefox

*** Test Cases ***
Login page test case
    # Implicit wait
    Set Selenium Implicit Wait    2 seconds
    # Open firefox and go to Cura Healthcare
    Open Browser    ${url}    ${browser}
    # Verifying title
    Title Should Be    CURA Healthcare Service
    # Maximize window, to be sure that everything goes well
    Maximize Browser Window
    # Click on button "Make an appointment"
        # Xpath version
        # Click Element    //a[@id='btn-make-appointment'] 
        # CSS selector version
        # Click Element    css:#btn-make-appointment
    Click Element    id:btn-make-appointment
    
    # Enter login and password
    Input Text    id:txt-username    John Doe
    Input Text    id:txt-password    ThisIsNotAPassword
    # Click login button
    Click Button    id:btn-login
    
    ### Filling form ###
    # Waiting for validation button to be visible
    Wait Until Element Is Visible    id:btn-book-appointment
    # Select Facility
    Click Element    //option[@value='Hongkong CURA Healthcare Center']
    # Tick Apply for hospital readmission
    Click Element    id:chk_hospotal_readmission
    # Click Medicaid
    Click Element    //input[@id='radio_program_medicaid']
    # Input date
    Input Text    id:txt_visit_date    02/07/2022
    # Enter a comment
    Input Text    id:txt_comment    Voici mon commentaire
    # Confirm appointment
    Click Button    id:btn-book-appointment
    # Go back to main page
    # Click Element    //li/a[@href='./']
    Click Element    //a[@href='https://katalon-demo-cura.herokuapp.com/']

    # Wait just to be able to see what's going on
    Sleep    2
    Close Browser